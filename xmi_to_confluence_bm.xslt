<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:uml="http://schema.omg.org/spec/UML/2.0" xmlns:xmi="http://schema.omg.org/spec/XMI/2.1">
    <xsl:output method="html" encoding="UTF-8" indent="yes"/>
    <xsl:template match="/">
	
		<meta http-equiv="Content-Type" content="text/html;charset=UTF-8"> 

        <p><h2>Begreppsmodell och beskrivning</h2></p>
        <p>Beskrivning av begrepp <br/>
        I detta avsnitt beskrivs de begrepp som används inom domänen. Begreppen kan sedan referera till klasser och attribut i informationsmodellen.
        Kolumnen "Relation till informationsmodell" visar relationen till klasser och attribut i informationsmodellen.</p> 
		<img src="../../images/Begreppsmodell.png" />
        <table border="1">
            <tbody align="left"><tr><th>Begrepp</th><th>Definition och referens till källa</th><th>Beskrivning av begreppen för framtagen begreppsmodell</th><th>Relation till informationsmodell</th></tr>
                <xsl:for-each select="/xmi:XMI/uml:Model[1]/ownedMember[@name = 'Terminology diagram']/ownedMember[@name='Begreppsmodell gemensam']/ownedMember[@xmi:type='uml:Class']">
					<xsl:sort select="./@name"/>
					<tr>
                        <td><xsl:value-of select="./@name"/></td>
                        <td><xsl:value-of select="xmi:Extension/taggedValue[@tag = 'TERMBANK']/@value"/></td>
                        <td><xsl:value-of select="ownedComment/body"/></td>
                        <td><xsl:value-of select="xmi:Extension/taggedValue[@tag = 'DIM']/@value"/></td>
                    </tr>
                </xsl:for-each>
            </tbody>
        </table>
     
		</meta>
    </xsl:template>
</xsl:stylesheet>
