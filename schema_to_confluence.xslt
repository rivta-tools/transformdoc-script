<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:iso="http://purl.oclc.org/dsdl/schematron">
    <xsl:output method="html"/>
    <xsl:template match="/">
        <table>
        <tbody><tr><th>Id</th><th>Beskrivning</th><th>Se även</th></tr>
            <xsl:for-each select="iso:schema/iso:pattern/iso:rule">
                <tr>
                    <td><xsl:value-of select="iso:assert/@id"/></td>
                    <td><xsl:value-of select="iso:assert"/></td>
                    <td><xsl:value-of select="iso:assert/@see"/></td>
                </tr>
            </xsl:for-each>
        </tbody>            
    </table>
    </xsl:template>
</xsl:stylesheet> 