<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:uml="http://schema.omg.org/spec/UML/2.0" xmlns:xmi="http://schema.omg.org/spec/XMI/2.1">
    <xsl:output method="html" encoding="UTF-8" indent="yes"/>
    <xsl:template match="/">
	
 <meta http-equiv="Content-Type" content="text/html;charset=UTF-8"> 

        <p><h2>Begreppsmodell och beskrivning</h2></p>
        <p>Beskrivning av begrepp <br/>
        I detta avsnitt beskrivs de begrepp som används inom domänen. Begreppen kan sedan referera till klasser och attribut i informationsmodellen.
        Kolumnen "Relation till informationsmodell" visar relationen till klasser och attribut i informationsmodellen.</p> 
		<img src="../../images/Begreppsmodell.png" />
        <table border="1">
            <tbody align="left"><tr><th>Begrepp</th><th>Definition och referens till källa</th><th>Beskrivning av begreppen för framtagen begreppsmodell</th><th>Relation till informationsmodell</th></tr>
                <xsl:for-each select="/xmi:XMI/uml:Model[1]/ownedMember[@name = 'Terminology diagram']/ownedMember[@name='Begreppsmodell gemensam']/ownedMember[@xmi:type='uml:Class']">
					<xsl:sort select="./@name"/>
					<tr>
                        <td><xsl:value-of select="./@name"/></td>
                        <td><xsl:value-of select="xmi:Extension/taggedValue[@tag = 'TERMBANK']/@value"/></td>
                        <td><xsl:value-of select="ownedComment/body"/></td>
                        <td><xsl:value-of select="xmi:Extension/taggedValue[@tag = 'DIM']/@value"/></td>
                    </tr>
                </xsl:for-each>
            </tbody>
        </table>
        <p><h2>Klasser och attribut</h2></p>
        <p>Detaljerad beskrivning av en klass och dess ingående attribut samt koppling mot E2B R2 standarden [se referens R1]. Då referensinformationsmodell saknas, finns ingen kolumn för detta med i tabellerna. 
        Varje klass beskrivs i ett underkapitel bokstavsordning efter namnet på klassen. Kolumnen 'Spårbarhet till krav' refererar till ett element i E2B-standarden om möjligt. Vissa attribut återfinns ej i E2B-standarden utan har tillkommit då behov finns från Läkemedelsverket att inhämta viss extra information (för exempelvis vidare uppföljning). 
        De datatyper som används är främst enumeration och string då det främst är textsträngar som Läkemedelsverket kan ta emot på sin sida i dagsläget.
        </p>
		<img src="../../images/Informationsmodell.png" />
		<table>
        <xsl:for-each select="/xmi:XMI/uml:Model[1]/ownedMember[@name='Information model']/ownedMember[@xmi:type='uml:Class']">
		<xsl:sort select="./@name"/>
		<tr><td><h3><xsl:value-of select="./@name"/></h3></td></tr>
        <tr><td><table border="1">
            <tbody align="left"><tr><th>Attribut</th><th>Mappning till RIM</th><th>Beskrivning</th><th>Datatyp</th><th>Multiplicitet</th><th>Kodverk/Format/Regler</th><th>Spårbarhet
                till krav</th></tr>
                <xsl:for-each select="./ownedAttribute">
				<xsl:sort select="./@name"/>
                    <tr>
                        <td><xsl:value-of select="./@name"/></td>
						<td><xsl:value-of select="xmi:Extension/taggedValue[@tag = 'RIM']/@value"/></td>
                        <td><xsl:value-of select="./ownedComment/body"/></td>
                        <td><xsl:value-of select="./@type"/></td>
						<td><xsl:value-of select="./lowerValue/@value"/><xsl:if test="./upperValue/@value != ''" >..<xsl:value-of select="./upperValue/@value"/></xsl:if></td>
                        <td><xsl:value-of select="xmi:Extension/taggedValue[@tag = 'KODVERK']/@value"/></td>
                        <td><xsl:value-of select="xmi:Extension/taggedValue[@tag = 'KRAV']/@value"/></td>
                    </tr>
                </xsl:for-each>
            </tbody>
        </table></td></tr>
		</xsl:for-each>
		</table>
		
		<xsl:for-each select="/xmi:XMI/uml:Model[1]/ownedMember[@xmi:type='uml:DataType']">
		<!--xsl:value-of select="./@name"/--><br/>
		</xsl:for-each>
		</meta>
    </xsl:template>
</xsl:stylesheet>
