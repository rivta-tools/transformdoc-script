<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:uml="http://schema.omg.org/spec/UML/2.0" xmlns:xmi="http://schema.omg.org/spec/XMI/2.1">
    <xsl:output method="html" encoding="UTF-8" indent="yes"/>
    <xsl:template match="/">
	
 <meta http-equiv="Content-Type" content="text/html;charset=UTF-8"> 

  
        <p><h2>Klasser och attribut</h2></p>
        <p>
        Varje klass beskrivs i ett underkapitel bokstavsordning efter namnet på klassen.
        </p>
		<img src="../../images/Informationsmodell.png" />
		<table>
        <xsl:for-each select="/xmi:XMI/uml:Model[1]/ownedMember[@name='Information model']/ownedMember[@xmi:type='uml:Class']">
		<xsl:sort select="./@name"/>
		<tr><td><h3><xsl:value-of select="./@name"/></h3></td></tr>
        <tr><td><table border="1">
            <tbody align="left"><tr><th>Attribut</th><th>Mappning till RIM</th><th>Beskrivning</th><th>Datatyp</th><th>Multiplicitet</th><th>Kodverk/Format/Regler</th><th>Spårbarhet
                till krav</th></tr>
                <xsl:for-each select="./ownedAttribute">
				<xsl:sort select="./@name"/>
                    <tr>
                        <td><xsl:value-of select="./@name"/></td>
						<td><xsl:value-of select="xmi:Extension/taggedValue[@tag = 'RIM']/@value"/></td>
                        <td><xsl:value-of select="./ownedComment/body"/></td>
                        <td><xsl:value-of select="./@type"/></td>
						<td><xsl:value-of select="./lowerValue/@value"/><xsl:if test="./upperValue/@value != ''" >..<xsl:value-of select="./upperValue/@value"/></xsl:if></td>
                        <td><xsl:value-of select="xmi:Extension/taggedValue[@tag = 'KODVERK']/@value"/></td>
                        <td><xsl:value-of select="xmi:Extension/taggedValue[@tag = 'KRAV']/@value"/></td>
                    </tr>
                </xsl:for-each>
            </tbody>
        </table></td></tr>
		</xsl:for-each>
		</table>
		</meta>
    </xsl:template>
</xsl:stylesheet>
