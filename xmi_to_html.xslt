<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:uml="http://schema.omg.org/spec/UML/2.0" xmlns:xmi="http://schema.omg.org/spec/XMI/2.1">
    <xsl:output method="html"/>
    <xsl:template match="/">
        <p>Begreppsmodell och beskrivning</p>
        <p>Beskrivning av begrepp 
        I detta avsnitt beskrivs de begrepp som används inom domänen. Begreppen kan sedan referera till klasser och attribut i informationsmodellen.
        Kolumnen "Relation till informationsmodell" visar relationen till klasser och attribut i informationsmodellen.</p>        
        <table>
            <tbody><tr><th>Begrepp</th><th>Definition</th><th>Beskrivning</th><th>Relation till informationsmodell</th></tr>
                <xsl:for-each select="/xmi:XMI/uml:Model[1]/ownedMember[8]/ownedMember">
                    <tr>
                        <td><xsl:value-of select="./@name"/></td>
                        <td><xsl:value-of select="xmi:Extension/taggedValue[@tag = 'TERMBANK']/@value"/></td>
                        <td><xsl:value-of select="ownedComment/body"/></td>
                        <td><xsl:value-of select="xmi:Extension/taggedValue[@tag = 'DIM-attribut']/@value"/></td>
                    </tr>
                </xsl:for-each>
            </tbody>
        </table>
        <p>Klasser och attribut</p>
        <p>Detaljerad beskrivning av en klass och dess ingående attribut samt koppling mot E2B R2 standarden [se referens R1]. Då referensinformationsmodell saknas, finns ingen kolumn för detta med i tabellerna. 
        Varje klass beskrivs i ett underkapitel bokstavsordning efter namnet på klassen. Kolumnen 'Spårbarhet till krav' refererar till ett element i E2B-standarden om möjligt. Vissa attribut återfinns ej i E2B-standarden utan har tillkommit då behov finns från Läkemedelsverket att inhämta viss extra information (för exempelvis vidare uppföljning). 
        De datatyper som används är främst enumeration och string då det främst är textsträngar som Läkemedelsverket kan ta emot på sin sida i dagsläget.
        </p>
        <table>
            <tbody><tr><th>Attribut</th><th>E2B</th><th>Beskrivning</th><th>Datatype(kard)</th><th>Kodverk/format</th><th>Spårbarhet
                till krav</th></tr>
                <xsl:for-each select="/xmi:XMI/uml:Model[1]/ownedMember[34]/ownedMember[20]/ownedAttribute">
                    <tr>
                        <td><xsl:value-of select="./@name"/></td>
                        <td><xsl:value-of select="ownedMember"/></td>
                        <td><xsl:value-of select="ownedMember"/></td>
                        <td><xsl:value-of select="ownedMember"/></td>
                        <td><xsl:value-of select="ownedMember"/></td>
                        <td><xsl:value-of select="ownedMember"/></td>
                    </tr>
                </xsl:for-each>
            </tbody>
        </table>
    </xsl:template>
</xsl:stylesheet>
